//
//  PlayerDetailsViewController.swift
//  RayWenderlich-Ratings
//
//  Created by Work on 01.04.19.
//  Copyright © 2019 Adrian Vogt. All rights reserved.
//

import UIKit


class PlayerDetailsViewController: UITableViewController {
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var detailLabel: UILabel!
  
  // MARK: - Properties
  var player: Player?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = false
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem
    
    objectToUI()
  }
  
  private func objectToUI() {
    if let player = player {
      if let game = player.game {
        detailLabel.text = game
      }
      if let name = player.name {
        nameTextField.text = name
      }
    }
  }
  
  private func objectFromUI() {
    if let playerName = nameTextField.text,
    let game = detailLabel.text {
      player = Player(name: playerName, game: game, rating: 1)
    }
  }
  
  // MARK: - Navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "SavePlayerDetail"{
      objectFromUI()
    }
    
    if segue.identifier == "PickGame",
      let game = detailLabel.text ,
      let gamePickerViewController = segue.destination as? GamePickerViewController {
      gamePickerViewController.selectedGame = game
    }
  }
  
}

// MARK: - IBActions
extension PlayerDetailsViewController {
  
  @IBAction func unwindWithSelectedGame(segue: UIStoryboardSegue) {
    if let gamePickerViewController = segue.source as? GamePickerViewController,
      let selectedGame = gamePickerViewController.selectedGame {
      detailLabel.text = selectedGame
    }
  }
}



// MARK: - UITableViewDelegate
extension PlayerDetailsViewController {
  
  // tap anywhere in the row to activate the text field
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.section == 0 {
      nameTextField.becomeFirstResponder()
    }
  }
}

