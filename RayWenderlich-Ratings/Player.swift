//
//  Player.swift
//  RayWenderlich-Ratings
//
//  Created by Work on 01.04.19.
//  Copyright © 2019 Adrian Vogt. All rights reserved.
//

import Foundation

struct Player {
  
  // MARK: - Properties
  var name: String?
  var game: String?
  var rating: Int
}
