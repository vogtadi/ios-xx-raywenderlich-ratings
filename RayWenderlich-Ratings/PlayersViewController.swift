//
//  PlayersViewController.swift
//  RayWenderlich-Ratings
//
//  Created by Work on 01.04.19.
//  Copyright © 2019 Adrian Vogt. All rights reserved.
//

import UIKit

class PlayersViewController: UITableViewController {
  
  // MARK: - Properties
  var players = SampleData.generatePlayersData()
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = false
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem
  }
}


// MARK: - IBActions
extension PlayersViewController {
  @IBAction func cancelToPlayersViewController(_ segue: UIStoryboardSegue) {
  }
  
  @IBAction func savePlayerDetail(_ segue: UIStoryboardSegue) {
    
    guard let playerDetailsViewController = segue.source as? PlayerDetailsViewController,
      let player = playerDetailsViewController.player else {
        return
    }
    
    tableView.beginUpdates()
    if let selection = tableView.indexPathForSelectedRow {
      // modify
      players[selection.row] = player
      
      // update the tableView
      let indexPath = IndexPath(row: selection.row, section: 0)
      tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    else {
      // add the new player to the players array
      players.append(player)
      
      // update the tableView
      let indexPath = IndexPath(row: players.count - 1, section: 0)
      tableView.insertRows(at: [indexPath], with: .automatic)
    }
    tableView.endUpdates()
  }
  
  
  
  // MARK: - Navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let navigationController = segue.destination as? UINavigationController
    if segue.identifier == "EditPlayer",
      let cell = sender as? PlayerCell,
      let player = cell.player,
      let playerDetailsViewController = navigationController?.topViewController as? PlayerDetailsViewController {
      playerDetailsViewController.player = player
    }
  }
  
  
}

// MARK: - UITableViewDataSource
extension PlayersViewController {
  // MARK: - Table view data source
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete implementation, return the number of rows
    return players.count
  }
  
  override func tableView(_ tableView: UITableView,
                          cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell", for: indexPath) as! PlayerCell
    
    let player = players[indexPath.row]
    cell.player = player
    return cell
  }
}
